<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="<c:url value="/resources/css/jquery-ui.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css" /> " type="text/css" rel="stylesheet" />
<link href="<c:url value="/resources/css/app.css" /> " type="text/css" rel="stylesheet" />